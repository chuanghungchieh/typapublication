//
//  AnnouncementDetailViewController.swift
//  typapublication
//
//  Created by CHUANG HUNG CHIEH on 2020/10/29.
//

import UIKit
import Foundation
import MaterialComponents.MaterialAppBar
import MaterialComponents.MaterialCards

class AnnouncementDetailViewController: UIViewController
{
    //色系
    @objc var containerScheme: MDCContainerScheming = {
        let containerScheme = MDCContainerScheme()
        containerScheme.colorScheme.primaryColor = UIColor(hexString: "#004fa2", alpha: 1)
        containerScheme.colorScheme.backgroundColor = .white
        return containerScheme
    }()
    
    //appBar
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.addChild(appBarViewController)
    }
    
    //appBar
    required init?(coder aDecoder: NSCoder) {
        //fatalError(“init(coder:) has not been implemented”)
        super.init(coder: aDecoder)
    }
    
    let appBarViewController = MDCAppBarViewController()
    var announcementId: Int = 0
    var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "桃園市藥師公會"
        
        appBarViewController.isTopLayoutGuideAdjustmentEnabled = false
        appBarViewController.headerView.minMaxHeightIncludesSafeArea = false
        
        //狀態列顏色
        UIApplication.shared.statusBarStyle = .lightContent
        //appBar 標體顏色
        self.appBarViewController.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor : UIColor.white ]
        
        appBarViewController.applyPrimaryTheme(withScheme: containerScheme)
        view.addSubview(appBarViewController.view)
        appBarViewController.didMove(toParent: self)
        
        
        self.navigationItem.leftBarButtonItem =
            UIBarButtonItem(title: "回列表", style: .done, target: nil, action: #selector(backListView))
        
        
    }
    
    @objc func backListView(sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @available(iOS 11, *)
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        
        var height = 0;
        
        if (view.safeAreaInsets.top == 20.0) {
            height = Int(view.safeAreaInsets.top - 20)
        }else{
            height = Int(view.safeAreaInsets.top)
        }
        
        let fullScreenSize = UIScreen.main.bounds.size
        let repo = AnnouncementRepository()
        let announcementItem = repo.GetAnnouncementInfo(announcementId: announcementId)
        
        
        scrollView = UIScrollView(frame: CGRect(x: 0, y: 80 + height, width:Int(fullScreenSize.width), height:Int(fullScreenSize.height) - (80 + height) ))
        
        view.addSubview(scrollView)
        
        var constraints = [NSLayoutConstraint]()
        
        let titleLabel = UILabel()
        titleLabel.frame.size.width = fullScreenSize.width
        titleLabel.text = announcementItem.title
        titleLabel.numberOfLines = 0
        titleLabel.font = titleLabel.font.withSize(24)
        titleLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        titleLabel.textColor = UIColor.black
        //titleLabel.textAlignment = .left
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.sizeToFit()
        scrollView.addSubview(titleLabel)
        
        
        let publishedDateLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 120, height: 40))
        publishedDateLabel.text = announcementItem.publishedDate
        publishedDateLabel.font = titleLabel.font.withSize(12)
        publishedDateLabel.textColor = UIColor.black
        publishedDateLabel.translatesAutoresizingMaskIntoConstraints = false
        scrollView.addSubview(publishedDateLabel)
        
        
        
        if(announcementItem.imageUrl != nil){
            
    
            let imageView = UIImageView()
            imageView.translatesAutoresizingMaskIntoConstraints = false
            imageView.frame.size.width = fullScreenSize.width
            imageView.frame.size.height = 500
            imageView.contentMode = .scaleAspectFit
            imageView.load(url: URL(string: announcementItem.imageUrl!)!)
            scrollView.addSubview(imageView)
            
            
            
            constraints.append(NSLayoutConstraint(item: publishedDateLabel,
                                                  attribute: .top,
                                                  relatedBy: .equal,
                                                  toItem: titleLabel,
                                                  attribute: .bottomMargin,
                                                  multiplier: 1,
                                                  constant: 10))
            
            
            constraints.append(NSLayoutConstraint(item: publishedDateLabel,
                                                  attribute: .right,
                                                  relatedBy: .equal,
                                                  toItem: self.view,
                                                  attribute: .rightMargin,
                                                  multiplier: 1,
                                                  constant: 0))
            
            
            constraints.append(NSLayoutConstraint(item: titleLabel,
                                                  attribute: .width,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .width,
                                                  multiplier: 1,
                                                  constant: -20))
            
            constraints.append(NSLayoutConstraint(item: titleLabel,
                                                  attribute: .centerX,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .centerX,
                                                  multiplier: 1,
                                                  constant: 0))
            
            constraints.append(NSLayoutConstraint(item: imageView,
                                                  attribute: .top,
                                                  relatedBy: .equal,
                                                  toItem: publishedDateLabel,
                                                  attribute: .bottomMargin,
                                                  multiplier: 1,
                                                  constant: 20))
            
            constraints.append(NSLayoutConstraint(item: imageView,
                                                  attribute: .width,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .width,
                                                  multiplier: 1,
                                                  constant: 0))
            
        } else if(announcementItem.content != nil){
            
            
            let contentLabel = UILabel()
            contentLabel.frame.size.width = fullScreenSize.width
            //contentLabel.frame.size.height = fullScreenSize.height
            contentLabel.text = announcementItem.content
            contentLabel.numberOfLines = 0
            //contentLabel.font = titleLabel.font.withSize(24)
            contentLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
            contentLabel.textColor = UIColor.black
            contentLabel.translatesAutoresizingMaskIntoConstraints = false
            contentLabel.textAlignment = NSTextAlignment.justified
            //contentLabel.textAlignment = .left
            contentLabel.sizeToFit()
            scrollView.addSubview(contentLabel)
            
            
            //ScrollView 裡面總高度
            let contentHeight = contentLabel.frame.size.height + titleLabel.frame.size.height + 20
            scrollView.contentSize = CGSize(width: fullScreenSize.width, height: contentHeight)
            
            
            constraints.append(NSLayoutConstraint(item: publishedDateLabel,
                                                  attribute: .top,
                                                  relatedBy: .equal,
                                                  toItem: titleLabel,
                                                  attribute: .bottomMargin,
                                                  multiplier: 1,
                                                  constant: 10))
            
            
            constraints.append(NSLayoutConstraint(item: publishedDateLabel,
                                                  attribute: .right,
                                                  relatedBy: .equal,
                                                  toItem: self.view,
                                                  attribute: .rightMargin,
                                                  multiplier: 1,
                                                  constant: 0))
            
            
            constraints.append(NSLayoutConstraint(item: titleLabel,
                                                  attribute: .width,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .width,
                                                  multiplier: 1,
                                                  constant: -20))
            
            constraints.append(NSLayoutConstraint(item: titleLabel,
                                                  attribute: .centerX,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .centerX,
                                                  multiplier: 1,
                                                  constant: 0))
            
            
            
            
            constraints.append(NSLayoutConstraint(item: contentLabel,
                                                  attribute: .top,
                                                  relatedBy: .equal,
                                                  toItem: publishedDateLabel,
                                                  attribute: .bottomMargin,
                                                  multiplier: 1,
                                                  constant: 20))
            
            constraints.append(NSLayoutConstraint(item: contentLabel,
                                                  attribute: .width,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .width,
                                                  multiplier: 1,
                                                  constant: -20))
            
            
            
            
            constraints.append(NSLayoutConstraint(item: contentLabel,
                                                  attribute: .centerX,
                                                  relatedBy: .equal,
                                                  toItem: scrollView,
                                                  attribute: .centerX,
                                                  multiplier: 1,
                                                  constant: 0))
            
        }
        
    
        
        
        NSLayoutConstraint.activate(constraints)
    }
}

extension UIColor {
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat
        
        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                print(data)
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        
                    
                        
                        self?.image = image
                    }
                }
            }
        }
    }
}

extension UIImageView {
    var contentClippingRect: CGRect {
        guard let image = image else { return bounds }
        guard contentMode == .scaleAspectFit else { return bounds }
        guard image.size.width > 0 && image.size.height > 0 else { return bounds }

        let scale: CGFloat
        if image.size.width > image.size.height {
            scale = bounds.width / image.size.width
        } else {
            scale = bounds.height / image.size.height
        }

        let size = CGSize(width: image.size.width * scale, height: image.size.height * scale)
        let x = (bounds.width - size.width) / 2.0
        let y = (bounds.height - size.height) / 2.0

        return CGRect(x: x, y: y, width: size.width, height: size.height)
    }
}
