//
//  AnnouncementListViewController.swift
//  typapublication
//
//  Created by CHUANG HUNG CHIEH on 2020/10/27.
//

import UIKit
import MaterialComponents.MaterialButtons
import MaterialComponents.MaterialAppBar
import MaterialComponents.MaterialList
import MaterialComponents.MaterialBottomNavigation
import MaterialComponents.MaterialBottomNavigation_Theming
import MaterialComponents.MaterialContainerScheme

class AnnouncementListViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, MDCBottomNavigationBarDelegate {
    
    //色系
    @objc var containerScheme: MDCContainerScheming = {
        let containerScheme = MDCContainerScheme()
        containerScheme.colorScheme.primaryColor = UIColor(hexString: "#004fa2", alpha: 1)
        containerScheme.colorScheme.backgroundColor = .white
        return containerScheme
    }()
    
    let appBarViewController = MDCAppBarViewController()
    private let baseCellIdentifier: String = "baseCellIdentifier"
    var fullScreenSize: CGSize!
    var announcementList: AnnouncementResult!
    var collectionView: UICollectionView!
    var userId: String? = nil
    var announcementId: Int = 0
    
    //appBar
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.addChild(appBarViewController)
    }
    
    //appBar
    required init?(coder aDecoder: NSCoder) {
        //fatalError(“init(coder:) has not been implemented”)
        super.init(coder: aDecoder)
    }
    
    let bottomNavBar = MDCBottomNavigationBar()
    let allTabBarItem = UITabBarItem(title: "全部", image: UIImage(named: "all"), tag: 0)
    let readedTabBarItem = UITabBarItem(title: "已讀", image: UIImage(named: "read"), tag: 1)
    let notReadTabBarItem = UITabBarItem(title: "未讀", image: UIImage(named: "notread"), tag: 2)
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
     
        
        //當外面傳 announcementId 時，announcementId 大於零時，再導到詳細頁面
        if(announcementId > 0){

            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let announcementDetailViewController = storyBoard.instantiateViewController(withIdentifier: "AnnouncementDetail") as! AnnouncementDetailViewController
            announcementDetailViewController.announcementId = announcementId
            announcementDetailViewController.modalPresentationStyle = .fullScreen
            
            //將 announcementId 歸零，避免回列表時，announcementId 大於零，又回到詳細頁面
            announcementId = 0
            present(announcementDetailViewController, animated: true, completion: nil)
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = containerScheme.colorScheme.backgroundColor
        self.title = "桃園市藥師公會"
        

        appBarViewController.isTopLayoutGuideAdjustmentEnabled = false
        appBarViewController.headerView.minMaxHeightIncludesSafeArea = false
        
        
        //appBarViewController.headerView.contentIsTranslucent = false
        //狀態列顏色
        UIApplication.shared.statusBarStyle = .lightContent
        
        
        //appBar 標體顏色
        self.appBarViewController.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor : UIColor.white ]
        
        appBarViewController.applyPrimaryTheme(withScheme: containerScheme)
        view.addSubview(appBarViewController.view)
        appBarViewController.didMove(toParent: self)
        
        
        //-------------------------------------------
        
        
        fullScreenSize =
            UIScreen.main.bounds.size
        
        
        
        //-----------------------------------------------------------
        let repo = AnnouncementRepository()
        announcementList = repo.GetAnnouncementList(userId: userId!, filter: "all")
        
        bottomNavBar.delegate = self
        // Always show bottom navigation bar item titles.
        bottomNavBar.titleVisibility = .always

        // Cluster and center the bottom navigation bar items.
        bottomNavBar.alignment = .justifiedAdjacentTitles
        //allTabBarItem.badgeValue = String(announcementList.allCount)
        //readedTabBarItem.badgeValue = String(announcementList.readedCount)
        notReadTabBarItem.badgeValue = String(announcementList.notReadCount)
        
        bottomNavBar.items = [ allTabBarItem, readedTabBarItem, notReadTabBarItem ]
        bottomNavBar.selectedItem = allTabBarItem
        
        bottomNavBar.applyPrimaryTheme(withScheme: containerScheme)
        view.addSubview(bottomNavBar)
        
        
        self.navigationItem.rightBarButtonItem =
            UIBarButtonItem(title: "登出", style: .done, target: nil, action: #selector(logout))
        
    }
    
    @objc func logout(sender: Any) {
        
        var repo = UserRepository()
        repo.RemoveUserInfo()
        
        
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "Login") as! ViewController
        
        viewController.modalPresentationStyle = .fullScreen
      
        self.present(viewController, animated: true, completion: nil)
    }
    
    //bottomBar
    func layoutBottomNavBar() {
        let size = bottomNavBar.sizeThatFits(view.bounds.size)
        var bottomNavBarFrame = CGRect(x: 0,
                                       y: view.bounds.height - size.height,
                                       width: size.width,
                                       height: size.height)
        if #available(iOS 11.0, *) {
            bottomNavBarFrame.size.height += view.safeAreaInsets.bottom
            bottomNavBarFrame.origin.y -= view.safeAreaInsets.bottom
        }
        bottomNavBar.frame = bottomNavBarFrame
    }
    
    //bottomBar
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        layoutBottomNavBar()
    }
    
    //bottomBar
    @available(iOS 11, *)
    override func viewSafeAreaInsetsDidChange() {
        super.viewSafeAreaInsetsDidChange()
        layoutBottomNavBar()
        
       
        let layout = UICollectionViewFlowLayout()
        
        
        // 設置 section 的間距 四個數值分別代表 上、左、下、右 的間距
        layout.sectionInset = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5);
        
        // 設置每一行的間距
        layout.minimumLineSpacing = 10
        
        layout.itemSize = CGSize(
            width: CGFloat(fullScreenSize.width) ,
            height: CGFloat(fullScreenSize.width)/7)
        
        
        
        var height = 0;
        
        if (view.safeAreaInsets.top == 20.0) {
            height = Int(view.safeAreaInsets.top - 20)
        }else{
            height = Int(view.safeAreaInsets.top)
        }
        
        collectionView = UICollectionView(frame: CGRect(
                                            x: 0, y: 40 + view.safeAreaInsets.top,
                                            width: fullScreenSize.width,
                                            height: fullScreenSize.height - CGFloat((140 + height))),
                                          collectionViewLayout: layout)
        
        collectionView.translatesAutoresizingMaskIntoConstraints = true
        collectionView.register(
            MDCSelfSizingStereoCell.self,
            forCellWithReuseIdentifier: baseCellIdentifier)
        
        // 設置委任對象
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .white
        
        // 加入畫面中
        self.view.addSubview(collectionView)
        
    }
    
    //bottomBar
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    //bottomBar
    func bottomNavigationBar(
        _ bottomNavigationBar: MDCBottomNavigationBar,
        didSelect item: UITabBarItem
    ) {
        let repo = AnnouncementRepository()
        
        if(item.tag == 0){
            announcementList = repo.GetAnnouncementList(userId: userId!, filter: "all")
        }
        
        if(item.tag == 1){
            announcementList = repo.GetAnnouncementList(userId: userId!, filter: "read")
        }
        
        if(item.tag == 2){
            announcementList = repo.GetAnnouncementList(userId: userId!, filter: "notread")
        }
        
        collectionView.reloadData()
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return announcementList.announcements.count;
    }
    
    internal func collectionView(_ collectionView: UICollectionView,
                                 didSelectItemAt indexPath: IndexPath) {
        
      
        
        let announcementItem  = announcementList.announcements[indexPath.row]
        
        if(!announcementItem.read){
            let alertController = MDCAlertController(title: "訊息", message: "是否閱讀此公告訊息?")
            let  oKaction = MDCAlertAction(title:"確定") { [self] (oKaction) in
                
                let repo = AnnouncementRepository()
                
                let result = repo.UpdateReadAnnouncementRecord(userId: self.userId!, announcementId: announcementItem.announcementId)
                
                if(result){
                   
                    //已讀加 1
                    self.announcementList.readedCount += 1
                    
                    //未讀減 1
                    self.announcementList.notReadCount -= 1
                    //self.readedTabBarItem.badgeValue = String(self.announcementList.readedCount)
                    self.notReadTabBarItem.badgeValue = String(self.announcementList.notReadCount)
 
                    //準備轉場到詳細頁面
                    self.performSegue(withIdentifier: "showDetail", sender: indexPath)
                }
                
            }
            
            let cancelAction = MDCAlertAction(title:"取消") { (action) in
                
            }
            
            alertController.addAction(cancelAction)
            alertController.addAction(oKaction)
            
            alertController.applyTheme(withScheme: containerScheme)
           
            present(alertController, animated:true, completion:nil)
        }else{
            
            self.performSegue(withIdentifier: "showDetail", sender: indexPath)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell =
            collectionView.dequeueReusableCell(
                withReuseIdentifier: baseCellIdentifier, for: indexPath)
            as! MDCSelfSizingStereoCell
        
       
        //cell.layer.borderWidth = 1
        
        
        cell.elevation = ShadowElevation(rawValue: 0)
        //水浪顏色
        cell.inkColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        
        
        let item = announcementList.announcements[indexPath.row]
        cell.titleLabel.numberOfLines = 1
        cell.titleLabel.font =  .systemFont(ofSize: 16)
        cell.titleLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
        cell.titleLabel.text = item.title
        cell.titleLabel.textColor = UIColor(hexString: "#A4A3A2")
        cell.detailLabel.text = item.publishedDate
        
        if(!item.read)
        {
            cell.titleLabel.textColor = UIColor(hexString: "#4A4948")
            let image = UIImage(named: "baseline-visibility-24px")!.alpha(0.8)
            
            cell.trailingImageView.image = image
        }
        
        let lineView = UIView(frame: CGRect(x: 0, y: 70, width: UIScreen.main.bounds.size.width, height: 1.0))
        lineView.layer.borderWidth = 1.0
        lineView.layer.borderColor = UIColor.systemGray.cgColor
        cell.addSubview(lineView)
    
        return cell
    }
    
    func numberOfSectionsInCollectionView(
        collectionView: UICollectionView) -> Int {
        return 1
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showDetail" {
            let destinationController = segue.destination as! AnnouncementDetailViewController
            let rowIndex = collectionView.indexPathsForSelectedItems![0].row
            destinationController.announcementId = announcementList.announcements[rowIndex].announcementId
            
            destinationController.modalPresentationStyle = .fullScreen
            
            //只能在這更新 CELL 已讀狀態
            announcementList.announcements[rowIndex].read = true
            collectionView.reloadItems(at: [collectionView.indexPathsForSelectedItems![0]])
        }
    }
}

extension UIColor {
    convenience init(hexString: String, alpha: CGFloat = 1.0) {
        let hexString: String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        var color: UInt32 = 0
        scanner.scanHexInt32(&color)
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
}

extension UIImage {

    func alpha(_ value:CGFloat) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(at: CGPoint.zero, blendMode: .normal, alpha: value)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
}
