//
//  AnnouncementRepository.swift
//  typapublication
//
//  Created by CHUANG HUNG CHIEH on 2020/10/27.
//
import Foundation

public class AnnouncementRepository {
    
    let _apiUrl = "https://app.pharmacist.org.tw"
    //let _apiUrl = "http://218.161.124.208/pub"
    
    init(){
        
    }
    
    internal func GetAnnouncementList(userId: String, filter: String) -> AnnouncementResult
    {
        var result: AnnouncementResult!
        let semaphore = DispatchSemaphore(value: 0)
        let url = URL(string:"\(_apiUrl)/api/announcement/getannouncementlist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        let parameters: [String: Any] = ["userId": userId, "filter": filter]
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions())
        }catch let error{
            print(error)
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            let decoder = JSONDecoder()
            
            
            decoder.dateDecodingStrategy = .iso8601
            if let data = data, let announcementResult = try?
                decoder.decode(AnnouncementResult.self, from: data)
            {
                
                result = announcementResult
               
                semaphore.signal()
                
            } else {
                print(error as Any)
                print("error")
            }
        }
        
        task.resume()
        semaphore.wait();
        return result
    }
    
    internal func GetAnnouncementInfo(announcementId: Int) -> AnnouncementInfo
    {
        var result: AnnouncementInfo!
        let semaphore = DispatchSemaphore(value: 0)
        let url = URL(string:"\(_apiUrl)/api/announcement/getannouncementinfo")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        //print("GetAnnouncementInfo=")
        print(String(announcementId))
        let parameters: [String: Any] = ["announcementId": announcementId]
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions())
        }catch let error{
            print(error)
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            let decoder = JSONDecoder()
            
           
            decoder.dateDecodingStrategy = .iso8601
            if let data = data, let AnnouncementInfo = try?
                decoder.decode(AnnouncementInfo.self, from: data)
            {
                result = AnnouncementInfo
                print(result.title)
                semaphore.signal()
                
            } else {
                print(error as Any)
                print("error")
            }
        }
        
        task.resume()
        semaphore.wait();
        return result
    }
    
    internal func UpdateReadAnnouncementRecord(userId: String, announcementId: Int) -> Bool
    {
        let semaphore = DispatchSemaphore(value: 0)
        let url = URL(string:"\(_apiUrl)/api/announcement/updateReadAnnouncementRecord")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let parameters: [String: Any] = ["userId": userId, "announcementId": announcementId]
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions())
        }catch let error{
            print(error)
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait();
        return true
    }
    
    internal func CreateUser(userId: String, name: String) -> Bool
    {
        var result: CreateUserResult!
        let semaphore = DispatchSemaphore(value: 0)
        let url = URL(string:"\(_apiUrl)/api/user/create")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        
        let parameters: [String: Any] = ["userId": userId, "name": name]
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions())
        }catch let error{
            print(error)
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            let decoder = JSONDecoder()
            
           
            decoder.dateDecodingStrategy = .iso8601
            if let data = data, let createUserResult = try?
                decoder.decode(CreateUserResult.self, from: data)
            {
                result = createUserResult
                semaphore.signal()
                
            } else {
                print(error as Any)
                print("error")
            }
        }
        
        task.resume()
        semaphore.wait();
        return result.success
    }
    
    
}

struct AnnouncementResult: Codable {
    
    struct AnnouncementItem: Codable {
        var title: String
        var publishedDate: String
        var read : Bool
        var announcementId: Int
    }
    
    var announcements: [AnnouncementItem]
    var allCount: Int
    var notReadCount: Int
    var readedCount: Int
}

struct AnnouncementInfo: Codable {
    var title: String
    var publishedDate: String
    var read : Bool
    var announcementId: Int
    var content: String?
    var imageUrl: String?
}

struct CreateUserResult: Codable {
    var success: Bool
}
