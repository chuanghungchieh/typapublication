//
//  AuthRepository.swift
//  typapublication
//
//  Created by CHUANG HUNG CHIEH on 2020/10/27.
//
import Foundation

public class AuthRepository {
    
    let _apiUrl = "https://app.pharmacist.org.tw/webAuth"
    
    init(){
        
    }
    
    internal func Auth(userId: String) -> AuthResult
    {
        var result: AuthResult! = AuthResult(succ: false, code: "", message: "", dataTime: "", data : [])
        let semaphore = DispatchSemaphore(value: 0)
        let url = URL(string:"\(_apiUrl)/api/webauth/GetQuizzes")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        let parameters: [String: Any] = ["userId": userId]
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions())
        } catch let error{
            print(error)
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            let decoder = JSONDecoder()
            
            decoder.dateDecodingStrategy = .iso8601
            if let data = data, let authResult = try?
                decoder.decode(AuthResult.self, from: data)
            {
                print(userId)
                print(authResult)
                result = authResult
               
                
            } else {
                print(error as Any)
                print("error")
            }
            
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait();
        return result
    }
    
    internal func Login(userId: String, answerInfo: [QuestionInfo]) -> LoginResult
    {
        var result: LoginResult! = LoginResult(succ: false, code: "", message: "", dataTime: "", data: nil )
        let semaphore = DispatchSemaphore(value: 0)
        let url = URL(string:"\(_apiUrl)/api/webauth/DoLogin")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        let parameters: [String: Any] = ["userId": userId, "answers": [
            ["questionID": answerInfo[0].questionID, "optionValue": answerInfo[0].optionValue],
            ["questionID": answerInfo[1].questionID, "optionValue": answerInfo[1].optionValue],
            ["questionID": answerInfo[2].questionID, "optionValue": answerInfo[2].optionValue]
        
        ]]
        
       
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options:[])
        }catch let error{
            print(error)
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            let decoder = JSONDecoder()
            
            decoder.dateDecodingStrategy = .iso8601
            
            if let data = data, let loginResult = try?
                decoder.decode(LoginResult.self, from: data)
            {
                result = loginResult
                
            } else {
                print(error as Any)
                print("error")
            }
            
            semaphore.signal()
        }
        
        task.resume()
        semaphore.wait();
        
        return result
    }
    
    internal func CheckLogin(userId: String, token: String) -> CheckLoginResult
    {
        var result: CheckLoginResult!
        let semaphore = DispatchSemaphore(value: 0)
        let url = URL(string:"\(_apiUrl)/api/webauth/CheckLogin")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        let parameters: [String: Any] = ["userId": userId, "token": token]
        
        do{
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions())
        }catch let error{
            print(error)
        }
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            let decoder = JSONDecoder()
            
            decoder.dateDecodingStrategy = .iso8601
            if let data = data, let checkLoginResult = try?
                decoder.decode(CheckLoginResult.self, from: data)
            {
                
                result = checkLoginResult
                semaphore.signal()
                
            } else {
                print(error as Any)
                print("error")
            }
        }
        
        task.resume()
        semaphore.wait();
        return result
    }
    
    
}

struct AuthResult: Codable  {
    struct QuizzeItem: Codable {
        
        struct Option: Codable {
            var option_text: String
            var option_value: String
        }
        
        var question_id: String
        var questionText: String
        var questionOptions: [Option]
        var selectedValue: String?
    }
    
    var succ: Bool
    var code: String
    var message: String?
    var dataTime: String
    var data: [QuizzeItem]?
}

struct LoginResult: Codable  {
    
    struct result: Codable {
        var userID: String
        var token: String
        var isLogin: Bool
    }
    
    var succ: Bool
    var code: String
    var message: String?
    var dataTime: String
    var data: result?
}

struct CheckLoginResult: Codable  {
    
    struct result: Codable {
        var cust_card_no: String
        var token: String
        var expired: Bool
        var logout: Bool
    }
    
    var succ: Bool
    var code: String
    var message: String?
    var dataTime: String
    var data: result
}


public class QuestionInfo{
    var questionID: String = ""
    var optionValue: String = ""
}
