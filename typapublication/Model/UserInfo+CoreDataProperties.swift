//
//  UserInfo+CoreDataProperties.swift
//  typapublication
//
//  Created by CHUANG HUNG CHIEH on 2020/12/6.
//
//

import Foundation
import CoreData


extension UserInfo {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<UserInfo> {
        return NSFetchRequest<UserInfo>(entityName: "UserInfo")
    }

    @NSManaged public var userId: String?
    @NSManaged public var token: String?

}
