//
//  Question_1_ViewController.swift
//  typapublication
//
//  Created by CHUANG HUNG CHIEH on 2020/11/30.
//

import UIKit
import Foundation
import MaterialComponents.MaterialAppBar
import MaterialComponents.MaterialCards

class Question_1_ViewController: UIViewController
{
    //色系
    @objc var containerScheme: MDCContainerScheming = {
        let containerScheme = MDCContainerScheme()
        containerScheme.colorScheme.primaryColor = UIColor(hexString: "#004fa2", alpha: 1)
        containerScheme.colorScheme.backgroundColor = .white
        return containerScheme
    }()
    
    //appBar
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.addChild(appBarViewController)
    }
    
    //appBar
    required init?(coder aDecoder: NSCoder) {
        //fatalError(“init(coder:) has not been implemented”)
        super.init(coder: aDecoder)
    }
    
    let appBarViewController = MDCAppBarViewController()
    var authResult: AuthResult? = nil
    var userId: String? = nil
    var buttons: [UIButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "問題1"
        
        appBarViewController.isTopLayoutGuideAdjustmentEnabled = false
        appBarViewController.headerView.minMaxHeightIncludesSafeArea = false
        
        //狀態列顏色
        UIApplication.shared.statusBarStyle = .lightContent
        
        //appBar 標體顏色
        self.appBarViewController.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor : UIColor.white ]
        
        appBarViewController.applyPrimaryTheme(withScheme: containerScheme)
        view.addSubview(appBarViewController.view)
        appBarViewController.didMove(toParent: self)
        
        
        
        let question = UILabel(frame: CGRect(x: 30, y: 130, width: UIScreen.main.bounds.size.width, height: 40))
        question.numberOfLines = 0
        question.text = authResult?.data![0].questionText
        view.addSubview(question)
        
        
        buttons = [UIButton]()
        //選項1
        let button1 = UIButton(frame: CGRect(x: 50, y: 200, width: 30, height: 30))
        button1.setTitleColor(UIColor.black, for: .normal)
        button1.setTitle("No", for: .normal)
        button1.setImage(UIImage(named: "radio-button-unselected")!, for: .normal)
        button1.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button1.setImage(UIImage(named: "radio-button-selected")!, for: .selected)
        view.addSubview(button1)
        buttons.append(button1)
        let label1 = UILabel(frame: CGRect(x: 100, y: 195, width: UIScreen.main.bounds.size.width - 50, height: 40))
        label1.numberOfLines = 0
        label1.text = authResult?.data![0].questionOptions[0].option_text
        view.addSubview(label1)
       
        //選項2
        let button2 = UIButton(frame: CGRect(x: 50, y: 300, width: 30, height: 30))
        button2.setTitleColor(UIColor.black, for: .normal)
        button2.setTitle("No", for: .normal)
        button2.setImage(UIImage(named: "radio-button-unselected")!, for: .normal)
        button2.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button2.setImage(UIImage(named: "radio-button-selected")!, for: .selected)
        view.addSubview(button2)
        buttons.append(button2)
        let label2 = UILabel(frame: CGRect(x: 100, y: 295, width: UIScreen.main.bounds.size.width - 50, height: 40))
        label2.numberOfLines = 0
        label2.text = authResult?.data![0].questionOptions[1].option_text
        view.addSubview(label2)
        
        //選項3
        let button3 = UIButton(frame: CGRect(x: 50, y: 400, width: 30, height: 30))
        button3.setTitleColor(UIColor.black, for: .normal)
        button3.setTitle("No", for: .normal)
        button3.setImage(UIImage(named: "radio-button-unselected")!, for: .normal)
        button3.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button3.setImage(UIImage(named: "radio-button-selected")!, for: .selected)
        view.addSubview(button3)
        buttons.append(button3)
        let label3 = UILabel(frame: CGRect(x: 100, y: 395, width: UIScreen.main.bounds.size.width - 50, height: 40))
        label3.numberOfLines = 0
        label3.text = authResult?.data![0].questionOptions[2].option_text
        view.addSubview(label3)
        
        //選項4
        let button4 = UIButton(frame: CGRect(x: 50, y: 500, width: 30, height: 30))
        button4.setTitleColor(UIColor.black, for: .normal)
        button4.setTitle("No", for: .normal)
        button4.setImage(UIImage(named: "radio-button-unselected")!, for: .normal)
        button4.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        button4.setImage(UIImage(named: "radio-button-selected")!, for: .selected)
        view.addSubview(button4)
        buttons.append(button4)
        let label4 = UILabel(frame: CGRect(x: 100, y: 495, width: UIScreen.main.bounds.size.width - 50, height: 40))
        label4.numberOfLines = 0
        label4.text = authResult?.data![0].questionOptions[3].option_text
        view.addSubview(label4)
        
        self.navigationItem.rightBarButtonItem =
            UIBarButtonItem(title: "下一題", style: .done, target: nil, action: #selector(nextQuestion))
        
    }
    
    @objc func nextQuestion(sender: Any) {
        if(authResult?.data![0].selectedValue == nil){
            let alertController = MDCAlertController(title: "訊息", message: "請選擇題目選項。")
            alertController.applyTheme(withScheme: containerScheme)
            let cancelAction = MDCAlertAction(title:"取消") { (action) in }
            alertController.addAction(cancelAction)
            present(alertController, animated:true, completion:nil)
        }else{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let question2Controller = storyBoard.instantiateViewController(withIdentifier: "Question_2") as! Question_2_ViewController
            
            question2Controller.modalPresentationStyle = .fullScreen
            question2Controller.authResult = authResult
            question2Controller.userId = userId
            self.present(question2Controller, animated: true, completion: nil)
            
        }
    }
    
    @objc func buttonAction(sender: UIButton!){
        for button in buttons {
            button.isSelected = false
        }
        sender.isSelected = true
       
        let buttonIndex = buttons.firstIndex{$0 === sender}
        let selectedValue = authResult?.data![0].questionOptions[buttonIndex!].option_value
        authResult?.data![0].selectedValue = selectedValue
    }
}
