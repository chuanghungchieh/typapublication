//
//  UserRepository.swift
//  typapublication
//
//  Created by CHUANG HUNG CHIEH on 2020/12/8.
//

import Foundation
import CoreData
import UIKit


public class UserRepository {
    
    
    public func SaveUserInfo(userId: String, token: String) {
        
        if #available(iOS 13.0, *) {
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                
                let request: NSFetchRequest<UserInfo> = UserInfo.fetchRequest()
                let context = appDelegate.persistentContainer.viewContext
                do {
                    let userInfos: [UserInfo]? = try context.fetch(request)
                    if let user = userInfos
                    {
                        if user.count > 0
                        {
                            context.delete(user.first!)
                        }
                        
                        let user = UserInfo(context: context)
                        user.userId = userId
                        user.token = token
                        appDelegate.saveContext()
                    }
                }catch{
                    print(error)
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    public func RemoveUserInfo() {
        
        if #available(iOS 13.0, *) {
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                
                let request: NSFetchRequest<UserInfo> = UserInfo.fetchRequest()
                let context = appDelegate.persistentContainer.viewContext
                do {
                    let userInfos: [UserInfo]? = try context.fetch(request)
                    if let user = userInfos
                    {
                        if user.count > 0
                        {
                            context.delete(user.first!)
                        }
                    }
                }catch{
                    print(error)
                }
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    public func GetUser() -> UserInfo? {
        
        if #available(iOS 13.0, *) {
            if let appDelegate = (UIApplication.shared.delegate as? AppDelegate){
                let request: NSFetchRequest<UserInfo> = UserInfo.fetchRequest()
                let context = appDelegate.persistentContainer.viewContext
                
                do {
                    let memberInfos: [UserInfo]? = try context.fetch(request)
                    if let members = memberInfos
                    {
                        if members.count > 0
                        {
                            return members.first!
                        }
                        else
                        {
                            return nil
                        }
                    }
                    
                }catch{
                    print(error)
                    return nil
                }
            }
        } else {
            // Fallback on earlier versions
        }
        return nil
    }
    
    
}
