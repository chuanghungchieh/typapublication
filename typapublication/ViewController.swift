//
//  ViewController.swift
//  typapublication
//
//  Created by CHUANG HUNG CHIEH on 2020/10/27.
//

import UIKit
import MaterialComponents.MaterialButtons
import MaterialComponents.MaterialTextControls_OutlinedTextFields

class ViewController: UIViewController, UITextFieldDelegate {
    
    //色系
    @objc var containerScheme: MDCContainerScheming = {
        let containerScheme = MDCContainerScheme()
        containerScheme.colorScheme.primaryColor = UIColor(hexString: "#004fa2", alpha: 1)
        containerScheme.colorScheme.backgroundColor = .white
        return containerScheme
    }()
    
    var textField: MDCOutlinedTextField!
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //狀態列顏色
        if #available(iOS 13.0, *) {
            UIApplication.shared.statusBarStyle = .darkContent
        } else {
            UIApplication.shared.statusBarStyle = .default
        }
        
        
        let logoImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        
        
        logoImageView.translatesAutoresizingMaskIntoConstraints = false;
        
        logoImageView.image = UIImage(named: "launchImage")
        
        self.view.addSubview(logoImageView)
        
        
        let nextButton: MDCButton = {
            let nextButton = MDCButton()
            nextButton.translatesAutoresizingMaskIntoConstraints = false
            nextButton.setTitle("登入", for: .normal)
            nextButton.setBackgroundColor(.black, for: .normal)
            nextButton.addTarget(self, action: #selector(didLogin(sender:)), for: .touchUpInside)
            return nextButton
        }()
        nextButton.applyContainedTheme(withScheme: containerScheme)
        self.view.addSubview(nextButton)
        
        
        textField = MDCOutlinedTextField(frame: CGRect(x: 0, y: 0, width: 200, height: 100))
        textField.label.text = "身份證號碼"
        textField.placeholder = "H123456789"
        textField.textAlignment = .center;
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.sizeToFit()
        textField.applyTheme(withScheme: containerScheme)
        self.view.addSubview(textField)
        textField.delegate = self;
        
        
        var constraints = [NSLayoutConstraint]()
        
        constraints.append(NSLayoutConstraint(item: logoImageView,
                                              attribute: .width,
                                              relatedBy: .equal,
                                              toItem: self.view,
                                              attribute: .width,
                                              multiplier: 0,
                                              constant: 200))
        
        constraints.append(NSLayoutConstraint(item: logoImageView,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: self.view,
                                              attribute: .height,
                                              multiplier: 0,
                                              constant: 200))
        
        constraints.append(NSLayoutConstraint(item: logoImageView,
                                              attribute: .centerX,
                                              relatedBy: .equal,
                                              toItem: self.view,
                                              attribute: .centerX,
                                              multiplier: 1,
                                              constant: 0))
        
        constraints.append(NSLayoutConstraint(item: logoImageView,
                                              attribute: .top,
                                              relatedBy: .equal,
                                              toItem: self.view,
                                              attribute: .topMargin,
                                              multiplier: 1,
                                              constant: 120))
        
        constraints.append(NSLayoutConstraint(item: textField!,
                                              attribute: .top,
                                              relatedBy: .equal,
                                              toItem: logoImageView,
                                              attribute: .bottomMargin,
                                              multiplier: 1,
                                              constant: 50))
        
        
        constraints.append(NSLayoutConstraint(item: textField!,
                                              attribute: .centerX,
                                              relatedBy: .equal,
                                              toItem: self.view,
                                              attribute: .centerX,
                                              multiplier: 1,
                                              constant: 0))
        
        
        constraints.append(NSLayoutConstraint(item: nextButton,
                                              attribute: .top,
                                              relatedBy: .equal,
                                              toItem: textField,
                                              attribute: .bottomMargin,
                                              multiplier: 1,
                                              constant: 30))
        
        
        constraints.append(NSLayoutConstraint(item: nextButton,
                                              attribute: .centerX,
                                              relatedBy: .equal,
                                              toItem: self.view,
                                              attribute: .centerX,
                                              multiplier: 1,
                                              constant: 0))
        
        constraints.append(NSLayoutConstraint(item: nextButton,
                                              attribute: .width,
                                              relatedBy: .equal,
                                              toItem: self.view,
                                              attribute: .width,
                                              multiplier: 0,
                                              constant: 200))
        
        constraints.append(NSLayoutConstraint(item: nextButton,
                                              attribute: .height,
                                              relatedBy: .equal,
                                              toItem: self.view,
                                              attribute: .height,
                                              multiplier: 0,
                                              constant: 50))
        
        
        NSLayoutConstraint.activate(constraints)
    }
    
    //按下 RETURN  就不顯示鍵盤
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @objc func didLogin(sender: Any) {
        
        
        
        let repo = AuthRepository()
        let result = repo.Auth(userId: textField.text!)
        
        if(textField.text! != ""){
            
            if(result.succ){
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let question1Controller = storyBoard.instantiateViewController(withIdentifier: "Question_1") as! Question_1_ViewController
                
                question1Controller.modalPresentationStyle = .fullScreen
                question1Controller.authResult = result
                question1Controller.userId = textField.text!
                self.present(question1Controller, animated: true, completion: nil)
            }else{
                let alertController = MDCAlertController(title: "訊息", message: "請輸入正確的身份證字號。")
                alertController.applyTheme(withScheme: containerScheme)
                let cancelAction = MDCAlertAction(title:"關閉") { (action) in }
                alertController.addAction(cancelAction)
                present(alertController, animated:true, completion:nil)
                alertController.applyTheme(withScheme: containerScheme)
            }
            
        }else{
            let alertController = MDCAlertController(title: "訊息", message: "請輸入正確的身份證字號。")
            alertController.applyTheme(withScheme: containerScheme)
            let cancelAction = MDCAlertAction(title:"關閉") { (action) in }
            alertController.addAction(cancelAction)
            present(alertController, animated:true, completion:nil)
            alertController.applyTheme(withScheme: containerScheme)
        }
        
        
    }
    
    
}

